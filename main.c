#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "AlgoGenetique.h"

int main(int argc, char *argv[])
{
	int opt, nbgeneration = 1;
	groupe population, parents;

	// les valeurs par defaut.
	population.nombre = NBPOPULATION;
	parents.nombre = NBPARENTS;
	int cible = CIBLE;
	int resultats = NBRESULTATS;
	int mutations = POURCENTAGEMUTATION;

	int nombre;
	while ((opt = getopt(argc, argv, "p:P:c:r:m:")) != -1)
	{
		switch (opt)
		{
		case 'p':
			if ((nombre = atoi(optarg)) != 0)
				population.nombre = nombre;
			break;
		case 'P':
			if ((nombre = atoi(optarg)) != 0)
				parents.nombre = nombre;
			break;
		case 'c':
			if ((nombre = atoi(optarg)) != 0)
				cible = nombre;
			break;
		case 'r':
			if ((nombre = atoi(optarg)) != 0)
				resultats = nombre;
			break;
		case 'm':
			if ((nombre = atoi(optarg)) != 0)
				mutations = nombre;
			break;
		default:
			fprintf(stderr, "Usage: %s [-p nbpopulation] [-P nbparents] [-c cible] [-r nbresultats] [-m %%mutations]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	// test et allocation mémoire.
	if (parents.nombre > population.nombre)
	{
		fprintf(stderr, "Le nombre de parents doit etre inferieur a la taille de la population\n");
		exit(EXIT_FAILURE);
	}
	if (cible > MAX || cible < -MAX)
	{
		fprintf(stderr, "La cible doit etre comprise entre %d et %d\n", MAX, -MAX);
		exit(EXIT_FAILURE);
	}
	if (resultats > population.nombre)
	{
		fprintf(stderr, "Le nombre de resultats doit etre inferieur a la taille de la population\n");
		exit(EXIT_FAILURE);
	}
	if (mutations > 100 || mutations < 0)
	{
		fprintf(stderr, "Le pourcentage de mutations doit etre compris entre 0 et 100\n");
		exit(EXIT_FAILURE);
	}
	if ((population.membres = malloc(sizeof(calcul) * population.nombre)) == NULL)
		exit(EXIT_FAILURE);
	if ((parents.membres = malloc(sizeof(calcul) * parents.nombre)) == NULL)
		exit(EXIT_FAILURE);

	// creation de la premiere génération
	generationAleatoire(&population);

	while (evaluation(&population, cible, resultats))
	{
		printf("\e[1;1H\e[2J");
		printf("\n ---------- Generation %d ---------- \n", nbgeneration);
		selection(&population, &parents);
		reproduction(&population, &parents);
		mutation(&population, mutations);
		nbgeneration++;
	}
	free(population.membres);
	free(parents.membres);
}
