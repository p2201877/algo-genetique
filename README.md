# Algo Genetique

Programme en C utilisant un algorithme génétique afin de touver des calculs donnant un certain résultat. Basé sur un TP effectué en cours en prépa.

## Fonctionnement

On génère aleatoirement des calculs composés d'octets contenant 2 genes chacun : le premier contient un nombre entre 0 et 16 et le deuxième une opération (+, -, *, /). La dernière opération n'est pas utilisée.
Pour chaque génération, on calcule la proximité du résultat de chaque calcul avec la cible. On selectionne les meilleurs et on les fait "se reproduire" en mixant leur gènes puis on effectue des mutations. Lorsque une génération a atteint un certain nombre de calcul donnant le résultat ciblé, le programme s'arrête (cela peut ne jamais arriver).

## Parametres

 - p le nombre de calcul dans chaque génération
 - P le nombre de calculs selectionnés dans chaque génération
 - c le résultat à atteindre
 - r le nombre de calculs valides dans un génération pour que le programme s'arrête
 - p le pourcentage de modifications a effectuer

## Améliorations possibles

 - ajout d'autres systèmes de selection (roulette, tournoi ...)
