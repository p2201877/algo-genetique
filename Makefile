AlgoGenetique : main.o AlgoGenetique.o
	gcc -o $@ $? -fsanitize=address -lm
	rm *.o
main.o : main.c 
	gcc -g -Wall -c main.c
AlgoGenetique.o : AlgoGenetique.c AlgoGenetique.h
	gcc -g -Wall -c AlgoGenetique.c
