#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "AlgoGenetique.h"

#define lire(gene, i) (i % 2) ? (gene[i / 2] & 0xF) : (gene[i / 2] >> 4)

/*
	Affiche un calcul
*/
void affiche(unsigned char *gene)
{
	char code[] = "+-*/";
	int i = 0, res;
	// le dernier gene est inutile
	while (i < (NBGENE - 1))
	{
		res = lire(gene, i);
		if (i % 2)
			printf("%c ", code[res % 4]);
		else
			printf("%d ", res);
		i = i + 1;
	}
}

/*
	Calcule le score d'un calcul
	les multiplications et divisions sont effectuées directement
	pour les additions et soustractions, on calcule les prochain termes (droite)
	et on les addition/soustrait avec le resultat actuel (gauche) au prochain +/- ou à la fin
	Le score est la difference entre le resultat du calculet la cible
*/
void calcule(calcul *g, int cible)
{
	unsigned char *gene = (unsigned char *)g;
	int gauche = 0;
	int droite = 1;
	int derniereOperation = 2;	// 0:+, 1:-, 2:*, 3:/
	int dernierPlusOuMoins = 0; // 0:+, 1:-
	for (int i = 0; i < NBGENE; i += 2)
	{
		int nombre = lire(gene, i);
		int operation = lire(gene, i / 2 + 1);
		operation %= 4;
		if (derniereOperation == 2)
		{
			droite *= nombre;
		}
		if (derniereOperation == 3)
		{
			if (nombre == 0)
			{
				g->score = MAX;
				return;
			}
			else
				droite /= nombre;
		}
		if (derniereOperation == 0)
		{
			if (dernierPlusOuMoins == 0)
				gauche += droite;
			else
				gauche -= droite;
			dernierPlusOuMoins = 0;
			droite = nombre;
		}
		if (derniereOperation == 1)
		{
			if (dernierPlusOuMoins == 0)
				gauche += droite;
			else
				gauche -= droite;
			dernierPlusOuMoins = 1;
			droite = nombre;
		}
		derniereOperation = operation;
	}
	if (dernierPlusOuMoins == 0)
		gauche += droite;
	else
		gauche -= droite;
	g->score = cible - gauche;
}

/*
	Fonction de test pour calcul()
*/
void testCalcul()
{
	int i, expect;
	calcul test[] = {
		{"\x67\xc6\x69\x73\x51\xff\x4a\xec\x29\xcd\xba\xab\xf2\xfb\xe3\x46\x7c\xc2\x54\xf8\x1b\xe8\xe7\x8d\x76\x5a\x2e\x63\x33\x9f\xc9\x9a", 660},
		{"\x66\x32\x0d\xb7\x31\x58\xa3\x5a\x25\x5d\x05\x17\x58\xe9\x5e\xd4\xab\xb2\xcd\xc6\x9b\xb4\x54\x11\x0e\x82\x74\x41\x21\x3d\xdc\x87", 735},
		{"\x70\xe9\x3e\xa1\x41\xe1\xfc\x67\x3e\x01\x7e\x97\xea\xdc\x6b\x96\x8f\x38\x5c\x2a\xec\xb0\x3b\xfb\x32\xaf\x3c\x54\xec\x18\xdb\x5c", 694},
		{"\x02\x1a\xfe\x43\xfb\xfa\xaa\x3a\xfb\x29\xd1\xe6\x05\x3c\x7c\x94\x75\xd8\xbe\x61\x89\xf9\x5c\xbb\xa8\x99\x0f\x95\xb1\xeb\xf1\xb3", 646},
		{"\x05\xef\xf7\x00\xe9\xa1\x3a\xe5\xca\x0b\xcb\xd0\x48\x47\x64\xbd\x1f\x23\x1e\xa8\x1c\x7b\x64\xc5\x14\x73\x5a\xc5\x5e\x4b\x79\x63", MAX},
		{"\x3b\x70\x64\x24\x11\x9e\x09\xdc\xaa\xd4\xac\xf2\x1b\x10\xaf\x3b\x33\xcd\xe3\x50\x48\x47\x15\x5c\xbb\x6f\x22\x19\xba\x9b\x7d\xf5", 543},
		{"\x0b\xe1\x1a\x1c\x7f\x23\xf8\x29\xf8\xa4\x1b\x13\xb5\xca\x4e\xe8\x98\x32\x38\xe0\x79\x4d\x3d\x34\xbc\x5f\x4e\x77\xfa\xcb\x6c\x05", 1302},
		{"\xac\x86\x21\x2b\xaa\x1a\x55\xa2\xbe\x70\xb5\x73\x3b\x04\x5c\xd3\x36\x94\xb3\xaf\xe2\xf0\xe4\x9e\x4f\x32\x15\x49\xfd\x82\x4e\xa9", MAX},
		{"\x08\x70\xd4\xb2\x8a\x29\x54\x48\x9a\x0a\xbc\xd5\x0e\x18\xa8\x44\xac\x5b\xf3\x8e\x4c\xd7\x2d\x9b\x09\x42\xe5\x06\xc4\x33\xaf\xcd", MAX},
		{"\xa3\x84\x7f\x2d\xad\xd4\x76\x47\xde\x32\x1c\xec\x4a\xc4\x30\xf6\x20\x23\x85\x6c\xfb\xb2\x07\x04\xf4\xec\x0b\xb9\x20\xba\x86\xc3", MAX},
		{"\x3e\x05\xf1\xec\xd9\x67\x33\xb7\x99\x50\xa3\xe3\x14\xd3\xd9\x34\xf7\x5e\xa0\xf2\x10\xa8\xf6\x05\x94\x01\xbe\xb4\xbc\x44\x78\xfa", 727}};

	for (i = 0; i < sizeof(test) / sizeof(calcul); i++)
	{
		expect = test[i].score;
		calcule(&test[i], CIBLE);
		if (expect != test[i].score)
			printf("erreur\n");
	}
}

/*
	Selectionne les (parents->nombre) calculs avec le score le plus proche de 0
*/
void selection(groupe *population, groupe *parents)
{
	printf("%d parents\n", parents->nombre);
	// on trie la population par score
	for (int i = 0; i < population->nombre; i++)
	{
		int min = i;
		for (int j = i + 1; j < population->nombre; j++)
		{
			if (abs(population->membres[j].score) < abs(population->membres[min].score))
				min = j;
		}
		calcul tmp = population->membres[i];
		population->membres[i] = population->membres[min];
		population->membres[min] = tmp;
	}
	// on copie les (parents->nombre) premiers calculs dans parents
	for (int i = 0; i < parents->nombre; i++)
		parents->membres[i] = population->membres[i];
}

/*
	Affiche les informations de la génération actuelle
*/
int evaluation(groupe *population, int cible, int resultats)
{
	int sommeScores = 0;
	int calculTrouve = resultats;
	for (int i = 0; i < population->nombre; i++)
	{
		calcule(&population->membres[i], cible);
		if (population->membres[i].score == 0)
		{
			if (calculTrouve > 0)
				calculTrouve--;
			// printf("calcul trouve : ");
			// affiche(population->membres[i].gene);
			// printf("\n");
		}
		if (population->membres[i].score != MAX)
			sommeScores += population->membres[i].score;
	}

	// calcule la moyenne et l'ecart type
	int moyenne = sommeScores / population->nombre;
	double variance = 0;
	for (int i = 0; i < population->nombre; i++)
		variance += pow(population->membres[i].score - moyenne, 2);
	variance /= population->nombre;
	double ecartType = sqrt(variance);
	printf("moyenne scores: %d\n", moyenne);
	printf("ecart type : %f\n", ecartType);

	printf("%d calculs valides trouves\n", resultats - calculTrouve);
	return calculTrouve;
}

/*
	Crée (population->nombre) calculs avec NBGENES aléatoires
*/
void generationAleatoire(groupe *population)
{
	for (int i = 0; i < population->nombre; i++)
	{
		for (int j = 0; j < NBGENE / 2; j++)
		{
			population->membres[i].gene[j] = rand() % 256;
			srand(time(NULL) * i * j);
			srand(rand());
		}
	}
}

/*
	Pour chaque enfant, on selectionne 2 parents au hasard et on effectue un cross-over pour créer le nouveau calcul
*/
void reproduction(groupe *population, groupe *parents)
{
	printf("%d enfants\n", population->nombre);
	for (int i = 0; i < population->nombre; i++)
	{
		int parent1 = rand() % parents->nombre;
		int parent2 = rand() % parents->nombre;
		int pointDeCroisement = rand() % NBGENE;
		calcul nouveauCalcul;
		// on copie les genes du parent 1 jusqu'au point de croisement
		for (int j = 0; j < pointDeCroisement / 2; j++)
			nouveauCalcul.gene[j] = parents->membres[parent1].gene[j];
		// on copie les genes du parent 2 a partir du point de croisement
		for (int j = pointDeCroisement / 2; j < NBGENE / 2; j++)
			nouveauCalcul.gene[j] = parents->membres[parent2].gene[j];
		population->membres[i] = nouveauCalcul;
	}
}

/*
	On selectionne des calculs au hasard
	pour chaque on selectionne un gene au hasard et on le modifie
*/
void mutation(groupe *population, int pourcentage)
{
	int nbMutations = pourcentage * NBGENE * population->nombre / 100;
	printf("%d mutations\n", nbMutations);
	for (int i = 0; i < nbMutations; i++)
	{
		int calcul = rand() % population->nombre;
		int geneAModifier = rand() % NBGENE;
		int gene = rand() % 256;
		srand(time(NULL) * i * geneAModifier);
		srand(rand());
		population->membres[calcul].gene[geneAModifier / 2] = gene;
	}
}