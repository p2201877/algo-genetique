#ifndef NBPOPULATION
#define NBPOPULATION 10000
#endif

#ifndef NBPARENTS
#define NBPARENTS 50
#endif

#ifndef MAX
#define MAX 2000000 // résultat max d'un calcul (en cas de division par 0)
#endif

#ifndef CIBLE
#define CIBLE 666 // résultat à atteindre
#endif

#ifndef POURCENTAGEMUTATION
#define POURCENTAGEMUTATION 2 // pourcentage de genes modifiés à chaque génération
#endif

#ifndef NBRESULTATS
#define NBRESULTATS 750 // nombre de résultat valides dans une génération pour arrêter le programme
#endif

#ifndef NBGENE
#define NBGENE 64
#endif

typedef struct
{
	unsigned char gene[NBGENE / 2];
	int score;
} calcul;

typedef struct
{
	calcul *membres;
	int nombre;
} groupe;

void affiche(unsigned char *gene);
void testCalcul();
void calcule(calcul *g, int cible);
void selection(groupe *population, groupe *parents);
int evaluation(groupe *population, int cible, int resultats);
void generationAleatoire(groupe *population);
void reproduction(groupe *population, groupe *parents);
void mutation(groupe *population, int pourcentage);
